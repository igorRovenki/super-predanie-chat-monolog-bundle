<?php

namespace PredanieChatMonologBundle\Manager;

class TelegramBotManager
{
    /** @var string */
    private $telegramBotToken;

    /** @var string */
    private $telegramChatId;

    public function __construct($telegramBotToken, $telegramChatId)
    {
       $this->telegramBotToken = $telegramBotToken;
       $this->telegramChatId = $telegramChatId;
    }

    /**
     * @param string $message
     */
    public function sendToTelegramBot($message)
    {
        $url = "https://api.telegram.org/bot" . $this->telegramBotToken . "/sendMessage?chat_id=" . $this->telegramChatId . "&text=" . $message;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $resp = curl_exec($curl);
        curl_close($curl);
    }
}
